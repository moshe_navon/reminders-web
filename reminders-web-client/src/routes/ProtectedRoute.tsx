import React, {useContext} from "react";
import {Route, RouteProps, Redirect} from "react-router-dom";
import {AppContext} from "../context/StoreProvider";

export default function ProtectedRoute(
    {component ,...props } : RouteProps,

) {

    const {user} = useContext(AppContext);

    return user ? (
        <Route {...props} component={component} />
    ) : (
        <Redirect to={{pathname :'login'}}/>
    )
}
