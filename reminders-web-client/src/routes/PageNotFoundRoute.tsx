import React, {useContext} from "react";
import {Route, Redirect} from "react-router-dom";
import {AppContext} from "../context/StoreProvider";
import {RouteComponentProps} from "react-router";

export default function ProtectedRoute(
    {...props},
    component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any> | undefined
) {

    const {user} = useContext(AppContext);

    return user ? (
        <Route {...props} component={component} render={undefined}/>
    ) : (
        <Redirect to={'login'}/>
    )
};
