export enum dateParseFormat {

    DATE = "YYYY-MM-DD",
    TIME = "HH:mm",
    EXTRA_TIME = 2

}