
export enum routesNames  {

    HOME = "/",
    LOGIN = "/login",
    PROFILE = "/profile",
    ANY = "*",

}