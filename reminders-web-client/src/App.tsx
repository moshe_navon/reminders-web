import './App.css';
import '@rocket.chat/icons/dist/rocketchat.css'
import {StoreProvider} from './context/StoreProvider'
import Home from "./views/home/HomeContainer";
import Profile from "./views/Profile/ProfileContainer";
import Login from "./views/login/LoginContainer";
import ProtectedRoute from "./routes/ProtectedRoute";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {routesNames} from "./enum/routesNames";

type routes = "login" | "profile" | ""

export default function App() {
    return (
        <StoreProvider>
                <Router>
                    <div id="App" className="App">
                        <header className="App-header">
                            <Switch>
                                <ProtectedRoute path={routesNames.HOME} exact component={Home}/>
                                <ProtectedRoute path={routesNames.PROFILE} exact component={Profile}/>
                                <Route exact path={routesNames.LOGIN}>
                                    <Login/>
                                </Route>
                                <Route path={routesNames.ANY}>
                                    <div>404</div>
                                </Route>
                            </Switch>
                        </header>
                    </div>
                </Router>
        </StoreProvider>
    );
}
