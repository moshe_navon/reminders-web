import {createContext} from 'react';
import {AppContextProps, FormProfile, Reminder, User, FormUser} from "../Types";
import {useStoreContext} from "./useStoreContext";

const defaultState: AppContextProps = {
    user: null,
    reminders: null,
    functions: null
};

export const AppContext = createContext<AppContextProps>(defaultState);

export const StoreProvider = ({children}: any) => {
    return (
        <AppContext.Provider value={useStoreContext()}>
            {children}
        </AppContext.Provider>
    );
};