import {useEffect, useState, useMemo} from 'react';
import {Reminder, User} from "../Types";
import {useSocketListener} from "../hooks/useSocketListener";
import {localStorageUser} from "../utils/localStorageUser";
import {appSocket} from "../AppSocket";
import {socketEmit} from "../utils/socketEmit";

export const useStoreContext = () => {
    const [reminders, setReminders] = useState<Reminder[] | undefined>();
    const [user, setUser] = useState<User | undefined | null>();

    const {loadUser} = localStorageUser()
    const {tryLogin, tryLogout, addReminder, updateReminder, deleteReminder, updateProfile} =
        socketEmit(appSocket, reminders, setReminders, user, setUser)
    useSocketListener(appSocket, reminders, setReminders, user, setUser)

    const remindersSort: Reminder[] | undefined = useMemo(() =>
            reminders?.sort((a: Reminder, b: Reminder) => a.calendar > b.calendar ? -1 : 1),
        [reminders]
    );

    useEffect(() => {
        const userFromLocale = loadUser()
        if (userFromLocale) {
            tryLogin(userFromLocale)
        }
    }, [])

    const functions = {
        updateReminder,
        addReminder,
        deleteReminder,
        updateProfile,
        tryLogin,
        tryLogout
    }

    return {user, functions, reminders: remindersSort}
};