import {Modal, TextInput, Field} from '@rocket.chat/fuselage'
import {ModalHeader} from '../../components/ModalHeader'
import {ButtonTooltip} from "../../components/ButtonTooltip";
import {FormProfile,  screenSize} from "../../Types";
import {ChangeEvent} from "react";

export default function Profile(
    screenSize: screenSize | null | undefined,
    form: FormProfile,
    handleChanges: (event: ChangeEvent<HTMLInputElement>) => void,
    isFormValid: boolean,
    updateButtonHandle: () => void, closeDialogHandle: () => void) {

    return (

        <div className="login-box">
            <Modal
                w={screenSize === 'mobile' ? '100vw' : screenSize === 'tablet' ? '50vw' : '30vw'}
                h={screenSize === 'mobile' ? '100vh' : 'auto'}
            >
                <ModalHeader title='פרטי משתמש' icon='arrow-back' tooltipText="חזור" onClick={() => closeDialogHandle()}/>
                <Modal.Content>
                    <Field>
                        <Field.Row>
                            <TextInput value={form.username} name="username" onChange={handleChanges}
                                       placeholder='שם משתמש'/>
                        </Field.Row>
                    </Field>
                </Modal.Content>
                <Modal.Footer>
                    <ButtonTooltip disabled={!isFormValid} tooltipText="שמור" width="100%" title="שמור" primary
                                   onClick={updateButtonHandle}/>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
