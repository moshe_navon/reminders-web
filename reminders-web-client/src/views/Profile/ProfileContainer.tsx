import {useContext} from 'react';
import {useHistory} from "react-router-dom";
import {AppContext} from "../../context/StoreProvider";
import {useForm} from "../../hooks/useForm";
import {useMediaQueries} from "../../hooks/useMediaQueries";
import {FormProfile} from "../../Types";
import Profile from './Profile';
import {useEffectOnChange} from "../../hooks/useEffectOnChange";

export default function ProfileContainer() {
    const {user, functions: fun} = useContext(AppContext);
    const {screenSize} = useMediaQueries()
    const history = useHistory()
    const validateProfileForm = () => {return form.username !== user?.username}

    const {form, isFormValid, handleChanges} = useForm<FormProfile>({
            formValues : user? {username: user?.username} : {username: ""},
            isDefaultValidate: true,
            customValidates: validateProfileForm
        })

    function updateButtonHandle() {
        fun?.updateProfile(form)
    }

    function closeDialogHandle() {
        history.goBack()
    }

    useEffectOnChange(() => {
        history.goBack()
    }, user);

    return Profile(screenSize, form, handleChanges, isFormValid, updateButtonHandle, closeDialogHandle)
}
