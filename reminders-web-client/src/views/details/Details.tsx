import './Details.css'
import {ModalHeader} from '../../components/ModalHeader'
import {Modal as ModalReact} from "react-responsive-modal";
import {ButtonTooltip} from "../../components/ButtonTooltip";
import {FormReminder} from "../../Types";
import moment from "moment";
import {dateParseFormat} from "../../enum/dateParseFormat";
import {
    FieldGroup,
    Modal,
    Field,
    InputBox,
    TextInput,
    TextAreaInput,
} from '@rocket.chat/fuselage'
import {ChangeEvent, MutableRefObject} from "react";

export default function Details(
    closeButtonHandle: () => void,
    handleChanges: (event: ChangeEvent<HTMLInputElement>) => void,
    form: FormReminder,
    isFormValid: Boolean,
    reminderId: string | undefined, handlePost: () => void,
    textInputRef: MutableRefObject<null>)
{
    return (
        <ModalReact
            open={true}
            onClose={closeButtonHandle}
            showCloseIcon={false}
            center
            initialFocusRef={textInputRef}
            classNames={{
                modal: 'customModal',
            }}>
            <div>
                <Modal>
                    <ModalHeader
                        title={!reminderId ? "התראה חדשה" : "ערוך התראה"}
                        tooltipText="סגור"
                        icon="cross"
                        onClick={closeButtonHandle}
                    />
                    <Modal.Content>
                        <FieldGroup>
                            <Field>
                                <Field.Row>
                                    <TextInput
                                        onChange={handleChanges}
                                        name="name"
                                        value={form.name}
                                        ref={textInputRef}
                                        placeholder='שם'
                                    />
                                </Field.Row>
                            </Field>
                            <Field>
                                <Field.Row>
                                    <TextAreaInput
                                        onChange={handleChanges}
                                        name="description"
                                        value={form.description}
                                        placeholder={'תיאור'}
                                    />
                                </Field.Row>
                            </Field>
                            <Field>
                                <Field.Row>
                                    <InputBox
                                        min={moment().format(dateParseFormat.DATE)}
                                        type="date"
                                        onChange={handleChanges}
                                        name="date"
                                        value={form.date}

                                    />
                                </Field.Row>
                            </Field>
                            <Field>
                                <Field.Row>
                                    <InputBox
                                        type="time"
                                        min={form.date === moment().format(dateParseFormat.DATE) ? moment().format(dateParseFormat.TIME) : ""}
                                        onChange={handleChanges}
                                        name="time"
                                        value={form.time}
                                    />
                                </Field.Row>
                            </Field>
                        </FieldGroup>
                    </Modal.Content>
                    <Modal.Footer>
                        <ButtonTooltip
                            disabled={!isFormValid}
                            tooltipText={!reminderId ? "הוסף התראה" : "שנה התראה"}
                            title={!reminderId ? " הוסף התראה " : " שנה התראה "}
                            icon="bell"
                            primary
                            fullWidth={true}
                            onClick={() => handlePost()}

                        />
                    </Modal.Footer>
                </Modal>
            </div>
        </ModalReact>
    );
}
