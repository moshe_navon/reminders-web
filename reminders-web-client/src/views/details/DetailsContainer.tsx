import {useContext, useEffect, useRef} from 'react';
import './Details.css'
import {AppContext} from "../../context/StoreProvider";
import {FormReminder, Reminder} from "../../Types";
import moment from "moment";
import {dateParseFormat} from "../../enum/dateParseFormat";
import {isPastDateReminder, isPastDateReminderForm} from "../../utils/datesParse";
import {v4 as uuidv4} from 'uuid';
import {useForm} from "../../hooks/useForm";
import {parseDateAndTimeToMomentObj} from "../../utils/datesParse";
import Details from "./Details";

interface addEditDialogProps {
    isOpen: boolean
    closeDialog: CallableFunction
    reminderId?: string
}

const defaultForm: FormReminder = {
    name: "",
    description: "",
    date: moment().format(dateParseFormat.DATE),
    time: moment().add(dateParseFormat.EXTRA_TIME, 'minutes').format(dateParseFormat.TIME)
}

const convertFormToReminder = (reminderForm: FormReminder, userId: string, reminderId?: string): Reminder => {
    const finalCalendar = parseDateAndTimeToMomentObj(reminderForm.date, reminderForm.time)

    return {
        name: reminderForm.name,
        description: reminderForm.description,
        calendar: finalCalendar,
        username: userId,
        id: reminderId ? reminderId : uuidv4(),
    }
}

const convertReminderToForm = (reminder: Reminder): FormReminder => {
    const date = !isPastDateReminder(reminder) ?
        moment(reminder.calendar).format(dateParseFormat.DATE) :
        moment().format(dateParseFormat.DATE)

    const time = !isPastDateReminder(reminder) ?
        moment(reminder.calendar).add(dateParseFormat.EXTRA_TIME, 'minutes').format(dateParseFormat.TIME) :
        moment().add(dateParseFormat.EXTRA_TIME, 'minutes').format(dateParseFormat.TIME)

    return {
        name: reminder.name,
        description: reminder.description,
        date,
        time
    }
}

const exceptValidate: string[] = ["description"]

export default function DetailsContainer({isOpen, closeDialog, reminderId}: addEditDialogProps) {
    const {reminders, user, functions} = useContext(AppContext);
    const {addReminder, updateReminder} = functions ?? {}

    const {form, isFormValid, initForm, handleChanges} = useForm<FormReminder>({
        formValues: defaultForm,
        isDefaultValidate: true,
        customValidates: () => !isPastDateReminderForm(form),
        exceptValidate
    })

    useEffect(() => {
        if (reminderId) {
            const reminder = reminders?.find((r: Reminder) => r.id === reminderId)
            if (reminder) initForm(convertReminderToForm(reminder))
        }
    }, [reminderId, reminders])

    const closeButtonHandle = () => {
        closeDialog()
        const date = moment().format(dateParseFormat.DATE)
        const time = moment().add(dateParseFormat.EXTRA_TIME, 'minutes').format(dateParseFormat.TIME)
        initForm({...form, date, time})
    }

    const handlePost = () => {
        if (isFormValid) {
            !reminderId ? addReminder?.(convertFormToReminder(form, user!.id)) : updateReminder?.(convertFormToReminder(form, user!.id, reminderId))
            initForm(defaultForm)
            closeDialog()
        }
    }

    const textInputRef = useRef(null);

    if (!isOpen) return null;
    return Details(closeButtonHandle, handleChanges, form, isFormValid, reminderId, handlePost, textInputRef)
}
