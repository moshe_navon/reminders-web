import {useContext, useState} from 'react';
import './Home.css';
import {useHistory} from "react-router-dom";
import {AppContext} from "../../context/StoreProvider";
import Home from './Home'

import {useMediaQueries} from "../../hooks/useMediaQueries";
import {routesNames} from "../../enum/routesNames";
import DetailsContainer from "../details/DetailsContainer";

export default function HomeContainer() {
    const {functions: fun, user} = useContext(AppContext);
    const [addEditDialogState, setAddEditDialog] = useState(false);
    const {screenSize} = useMediaQueries()
    const history = useHistory()

    function profileButtonHandle() {

    history.push(routesNames.PROFILE)
    }

    const logout = () => {
        fun?.tryLogout()
    };

    return Home(user, screenSize, profileButtonHandle, logout, setAddEditDialog, addEditDialogState)
}
