import './Home.css';
import ReminderTable from "./ReminderTable/ReminderTableContainer";
import ReactTooltip from 'react-tooltip';
import {Box, Tile} from '@rocket.chat/fuselage'
import Details from "../details/DetailsContainer";
import {ButtonTooltip} from "../../components/ButtonTooltip";
import {screenSize, User} from "../../Types";

export default function Home(
    user: User | null | undefined,
    screenSize: screenSize | null | undefined,
    profileButtonHandle: () => void,
    logout: () => void,
    setAddEditDialog: (value: (((prevState: boolean) => boolean) | boolean)) => void,
    addEditDialogState: boolean)
{
    return (
        <div className="main-view">
            <Details
                isOpen={addEditDialogState}
                closeDialog={() => setAddEditDialog(false)}/>

            <Tile
                w={screenSize === 'mobile' ? '100vw' : screenSize === 'tablet' ? '50vw' : '30vw'}
                h={screenSize === 'mobile' ? '100vh' : ''}
                display='flex'
                flexDirection='column'
                alignItems='center'
            >
                <header>
                    <ButtonTooltip
                        tooltipText="הוסף התראה"
                        onClick={() => setAddEditDialog(true)}
                        icon="plus"
                        primary/>

                    <Box fontFamily='sans' fontWeight={900} fontSize='x24'>
                        שלום&nbsp;
                        <a data-tip="שינוי שם משתמש" onClick={profileButtonHandle}>{user?.username}</a>
                        <ReactTooltip place="top" type="dark" effect="solid"/>
                    </Box>

                    <ButtonTooltip
                        onClick={logout}
                        tooltipText="התנתק"
                        icon="exit"
                        primary
                        danger/>
                </header>
                <ReminderTable/>
            </Tile>
        </div>
    );
}
