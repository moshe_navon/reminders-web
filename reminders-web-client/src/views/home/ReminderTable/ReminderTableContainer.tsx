import {useContext, useState} from 'react';
import './ReminderTable.css'
import {AppContext} from "../../../context/StoreProvider";
import ReminderTable from "./ReminderTable";
import DetailsContainer from "../../details/DetailsContainer";

export default function ReminderTableContainer() {
    const {reminders, functions: fun} = useContext(AppContext);
    const [isDetailsDialogOpen, setIsDetailsDialogOpen] = useState<boolean>(false);
    const [reminderSelectedId, setReminderSelectedId] = useState<string>();

    function handleClickReminder(reminderId: string) {
        setIsDetailsDialogOpen(!isDetailsDialogOpen)
        setReminderSelectedId(reminderId)
    }

    function handleClickDelete(reminderId: string) {
        fun?.deleteReminder(reminderId)
    }

    return ReminderTable(
        reminders,
        handleClickReminder,
        handleClickDelete,
        reminderSelectedId,
        isDetailsDialogOpen,
        setIsDetailsDialogOpen
    )
}