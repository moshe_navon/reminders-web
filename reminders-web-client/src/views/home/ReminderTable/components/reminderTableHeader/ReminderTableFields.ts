import {fieldsTableCol} from "../../../../../Types";

export const ReminderTableField: fieldsTableCol[] = [
    {
        name: "name",
        displayName: "שם",
        align: 'end'

    },
    {
        name: "description",
        displayName: "תיאור",
        align: 'end'

    },
    {
        name: "date",
        displayName: "תאריך",
        align: 'center'

    },
    {
        name: "time",
        displayName: "שעה",
        align: 'center'
    },

    {
        name: "delete",
        displayName: "",
        align: 'end'
    },
]
