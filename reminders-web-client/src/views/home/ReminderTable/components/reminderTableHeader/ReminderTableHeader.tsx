import {Table} from '@rocket.chat/fuselage'
import '../../ReminderTable.css'
import {ReminderTableField} from "./ReminderTableFields";

export default function ReminderTableHeader() {
    return (
        <Table.Head >
            <Table.Row >
                {
                    ReminderTableField.map(
                            (value) =>
                                <Table.Cell key={value.name} align={value.align}  fontScale='s2'>{value.displayName}</Table.Cell>
                        )
                }
            </Table.Row>
        </Table.Head>
    );
}