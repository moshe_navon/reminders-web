import {memo} from 'react';
import {Table} from '@rocket.chat/fuselage'
import moment from "moment";
import './ReminderItem.css'
import {ButtonTooltip} from "../../../../../components/ButtonTooltip";
import {Reminder} from "../../../../../Types";
import {dateParseFormat} from "../../../../../enum/dateParseFormat";

export interface ReminderItemProps  {
    reminder?: Reminder
    onClickReminder: CallableFunction
    onClickDelete: CallableFunction
}

export const ReminderItem = memo<ReminderItemProps>(({reminder, onClickReminder, onClickDelete}) => {
    return (
        <Table.Row className={"table-row"}
                   data-tip={"ערוך התראה"}
                   key={reminder?.id}
        >
            <Table.Cell
                onClick={() => {
                    onClickReminder(reminder?.id)
                }}
                is='th'
                scope='row'
                align='end'
            >
                {reminder?.name}
            </Table.Cell>
            <Table.Cell
                onClick={() => {
                    onClickReminder(reminder?.id)
                }}
                align='end'
            >
                {reminder?.description}
            </Table.Cell>
            <Table.Cell
                onClick={() => {
                    onClickReminder(reminder?.id)
                }}
            >
                {moment(reminder?.calendar).format(dateParseFormat.DATE)}
            </Table.Cell>
            <Table.Cell
                onClick={() => {
                    onClickReminder(reminder?.id)
                }}
            >
                {moment(reminder?.calendar).format(dateParseFormat.TIME)}

            </Table.Cell>
            <Table.Cell
                align='end'
            >
                <ButtonTooltip tooltipText="מחק תזכורת" icon="trash" ghost danger
                               onClick={() =>
                                   onClickDelete(reminder!.id)
                               }
                />
            </Table.Cell>
        </Table.Row>
    );
}, ((prevProps, nextProps) =>
        prevProps.reminder?.id === nextProps.reminder?.id &&
        prevProps.reminder?.name === nextProps.reminder?.name &&
        prevProps.reminder?.description === nextProps.reminder?.description &&
        prevProps.reminder?.calendar === prevProps.reminder?.calendar
));

export default ReminderItem;