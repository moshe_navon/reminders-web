import {Table} from '@rocket.chat/fuselage'
import './ReminderTable.css'
import Details from "../../details/DetailsContainer";
import ReminderTableHeader from "./components/reminderTableHeader/ReminderTableHeader";
import ReminderItem from "./components/reminderItem/ReminderItem";
import {Reminder} from "../../../Types";

export default function ReminderTable(
    reminders: Reminder[] | null | undefined,
    handleClickReminder: (reminderId: string) => void,
    handleClickDelete: (reminderId: string) => void,
    reminderSelectedId: string | undefined, isDetailsDialogOpen: boolean,
    setIsDetailsDialogOpen: (value: (((prevState: boolean) => boolean) | boolean)) => void) {
    return (
        <>
            {reminders ?
                (reminders.length > 0 ?
                    (
                        <Table fixed striped sticky>
                            <ReminderTableHeader/>
                            <Table.Body p="x16">
                                {reminders
                                    .map((reminder) =>
                                        <ReminderItem
                                            key={reminder.id}
                                            onClickReminder={handleClickReminder}
                                            onClickDelete={handleClickDelete}
                                            reminder={reminder}
                                        />
                                    )}
                            </Table.Body>
                        </Table>
                    )
                    : (<div><h3>אהלן :-) מה תרצה שנזכיר לך?</h3></div>))
                : <div><h3>טוען תזכורות....</h3></div>
            }

            <Details
                reminderId={reminderSelectedId}
                isOpen={isDetailsDialogOpen}
                closeDialog={() => setIsDetailsDialogOpen(false)}
            />
        </>
    );
}