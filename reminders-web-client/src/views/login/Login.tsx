import './Login.css';
import {ReactComponent as LogoSvg} from '../../reminders-logo1-01.svg';
import {ButtonTooltip} from "../../components/ButtonTooltip";
import {TextInput, Tile, FieldGroup, Field, PasswordInput} from '@rocket.chat/fuselage'
import {ChangeEvent} from "react";

export default function Login(
    handleChanges: (event: ChangeEvent<HTMLInputElement>) => void,
    isFormValid: boolean,
    loginButtonHandle: () => void,
) {
    return (
        <div className="login-view">
            <LogoSvg className={"App-logo"}/>
            <Tile className="login-box" display='flex' flexDirection='column' alignItems='center'
                  justifyContent='space-around'>
                <FieldGroup width="100%">

                    <Field>
                        <Field.Row>
                            <TextInput name="username" onChange={handleChanges} placeholder='שם משתמש'/>
                        </Field.Row>
                    </Field>
                    <Field>
                        <Field.Row>
                            <PasswordInput name="password" onChange={handleChanges} placeholder='סיסמא'/>
                        </Field.Row>
                    </Field>
                </FieldGroup>

                <ButtonTooltip
                    disabled={!isFormValid}
                    fullWidth={true}
                    tooltipText="התחבר"
                    title="התחבר"
                    mbs="x32"
                    primary
                    onClick={loginButtonHandle}/>
            </Tile>
        </div>
    )
}