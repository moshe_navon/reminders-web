import {useContext, useEffect} from 'react';
import './Login.css';
import {AppContext} from "../../context/StoreProvider";
import {useHistory} from "react-router-dom";
import {useForm} from "../../hooks/useForm";
import {FormUser} from "../../Types";
import {routesNames} from "../../enum/routesNames";
import Login from "./Login";

const defaultForm: FormUser = {
    username: "",
    password: ""
}

export default function LoginContainer() {
    const {user, functions: fun} = useContext(AppContext);
    const history = useHistory()
    const {isFormValid, handleChanges, form} = useForm<FormUser>({
        formValues: defaultForm,
        isDefaultValidate: true
    })

    function loginButtonHandle() {
        fun?.tryLogin(form)
    }

    useEffect(() => {
        console.log(process.env.REACT_APP_SOCKET_URL)
    }, [])

    useEffect(() => {
        if (user) history.push(routesNames.HOME)
    }, [user])

    return Login(handleChanges, isFormValid, loginButtonHandle)
}