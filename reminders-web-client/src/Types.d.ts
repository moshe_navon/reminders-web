import {ComponentProps} from "react";
import {SocketEvevts} from "./enum/socketEvevts";

interface FunctionsAppContextType {
    tryLogin: (user: FormUser) => void;
    updateProfile: (username: FormProfile) => void;
    addReminder: (reminder: Reminder) => void;
    updateReminder: (reminder: Reminder) => void;
    deleteReminder: (id: string) => void;
    tryLogout: () => void;
}

interface AppContextProps {
    reminders?: Reminder[] | null | undefined,
    user: User | null | undefined,
    functions: FunctionsAppContextType | null | undefined
}

type LoginResponse = SocketEvevts.LOGIN_SUCCESS | SocketEvevts.LOGIN_FAILED

type screenSize = "mobile" | "tablet" | "desktop" | null

interface fieldsTableCol {
    name: string,
    displayName: string,
    align?: "end" | "center" | "start",
}

export type ButtonTooltipProps = ComponentProps<typeof Button> & {
    tooltipText?: string;
    icon?: string;
    title?: string;
};

export interface User extends FormUser{
    id: string
}

export interface FormUser {
    username: string,
    password: string,
}

export interface FormProfile {
    username: string
}

export interface FormReminder {
    name: string,
    description: string
    date: string,
    time: string,
}

export interface BaseReminder extends Omit<FormReminder, "date" | "time"> {
    id: string
    username: string,
}

export interface Reminder extends BaseReminder{
    calendar: number,
}

export interface ReminderServer  extends BaseReminder{
    calendar: {
        year: number,
        month: number,
        dayOfMonth: number,
        hourOfDay: number,
        minute: number,
    }
}