import React from 'react';
import {Modal} from '@rocket.chat/fuselage'
import {ButtonTooltip} from "./ButtonTooltip";
import {ButtonTooltipProps} from "../Types";

export const ModalHeader: React.FC<ButtonTooltipProps> = ({tooltipText,icon, title, onClick}) =>
    <Modal.Header>
        <ButtonTooltip tooltipText={tooltipText} icon={icon} ghost onClick={onClick} />
            <Modal.Title fontFamily='sans' fontWeight={900} fontSize='x24'>{
                title
            }</Modal.Title>
    </Modal.Header>
