import {FC}  from 'react';
import {Button, Icon} from '@rocket.chat/fuselage'
import ReactTooltip from "react-tooltip";
import {ButtonTooltipProps} from "../Types";

export const ButtonTooltip: FC<ButtonTooltipProps> = ({tooltipText, icon, title, fullWidth = false, ...props}) =>
    <>
    <Button width={fullWidth ? "100%" : 'auto'} data-tip={tooltipText}  {...props} >
        {icon ? <Icon name={icon}/>: <></>}
        {title}
        </Button>
        <ReactTooltip place="top" type="dark" effect="solid"/>
    </>
