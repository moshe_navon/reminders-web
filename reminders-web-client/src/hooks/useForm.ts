import React, {useEffect, useState} from "react";
export type useFormPropsType<T> = {
    formValues: T
    exceptValidate?: string[]
    customValidates?: CallableFunction | CallableFunction[]
    isDefaultValidate?: boolean
};

export const useForm = <T extends object>(
    {formValues, isDefaultValidate, exceptValidate, customValidates} : useFormPropsType<T>
) => {
    const [form, setForm] = useState<T>(formValues)
    const [isFormValid, setIsFormValid] = useState<boolean>(false)

    useEffect(() => {
        let isValid = true
        if (isDefaultValidate) {
            isValid = isValid && defaultValidate()
        }

        if (customValidates instanceof Function) {
            isValid = isValid && customValidates()
        }
        if (customValidates instanceof Array) {
            customValidates.forEach((validate) => isValid = isValid && validate())
        }

        setIsFormValid(isValid)
    }, [form])

    const initForm = (obj: T): void => {
        setForm(obj)
    }

    const defaultValidate = (): boolean => {
        let isValid = true
        isValid = isValid && Object.keys(form)
            .every((key: string) => {
                    const value = (form[key as keyof T] as unknown)
                    if (!exceptValidate?.includes(key)) {
                        if (typeof value === "string") {
                            return !!value.trim()
                        }
                    } else {
                        return true
                    }
                }
            )

        return isValid
    }

    return {
        form,
        isFormValid,
        initForm,
        handleChanges: (event: React.ChangeEvent<HTMLInputElement>) => {
            setForm({
                ...form,
                [event.target.name]: event.target.value

            })
        }
    }
}