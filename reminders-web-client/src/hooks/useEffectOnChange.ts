import {useEffect, useRef} from 'react';

export const useEffectOnChange = (callback : CallableFunction, dependencies : any) => {
        const didMount = useRef(false);

        useEffect(() => {

            if (didMount.current) {
                callback(dependencies);
            } else {
                didMount.current = true;
            }
        }, [dependencies]);
    };
