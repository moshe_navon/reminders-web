import {useEffect, useState} from 'react';
import {screenSize} from "../Types";

export const useMediaQueries = () => {
    const mobileMatch = window.matchMedia('(max-width: 576px)');
    const tabletMatch = window.matchMedia('screen and (max-width: 1000px) and (min-width: 576px)');
    const desktopMatch = window.matchMedia('(min-width: 1000px)');
    const [screenSize, setScreenSize] = useState<screenSize>();

    useEffect(() => {
        if (window.innerWidth < 576) {
            setScreenSize("mobile")
        } else if (window.innerWidth < 1000 && window.innerWidth > 576) {
            setScreenSize("tablet")
        } else {
            setScreenSize("desktop")
        }
    },[])

    useEffect(() => {


        const mobileHandler = (e: any) => {
            if (e.matches) setScreenSize("mobile")
        };
        const tabletHandler = (e: any) => {
            if (e.matches) setScreenSize("tablet")
        };
        const desktopHandler = (e: any) => {
            if (e.matches) setScreenSize("desktop")
        };

        mobileMatch.addListener(mobileHandler);
        tabletMatch.addListener(tabletHandler);
        desktopMatch.addListener(desktopHandler);

        return () => {
            mobileMatch.removeListener(mobileHandler)
            tabletMatch.removeListener(tabletHandler)
            desktopMatch.removeListener(desktopHandler)
        };
    });
    return {screenSize: screenSize};
};