import {useEffect} from "react";
import {Reminder, ReminderServer, User} from "../Types";
import {SocketEvevts} from "../enum/socketEvevts";
import {parseCalendarToMilliseconds} from "../utils/datesParse";
import {localStorageUser} from "../utils/localStorageUser";
import {Socket} from "socket.io-client";

export const useSocketListener = (
    socket: Socket,
    reminders: Reminder[] | undefined,
    setReminders: Function,
    user: User | undefined | null,
    setUser: Function
) => {
    const {updateUser} = localStorageUser()

    useEffect(() => {
        socket.on(SocketEvevts.ADDED_REMINDER, addedReminder)
        socket.on(SocketEvevts.RECEIVE_REMINDERS, receiveReminders)
        socket.on(SocketEvevts.CHANGE_USERNAME_SUCCESS, changeUsernameSuccess)
        socket.on(SocketEvevts.CHANGE_USERNAME_FAILED, changeUsernameFailed)
        socket.on(SocketEvevts.DELETED_REMINDER, deletedReminder)
        socket.on(SocketEvevts.UPDATED_REMINDER, updatedReminder)
        return () => {
            socket.off()
        }
    }, [])

    const changeUsernameFailed = () => {
        alert('Username already in use')
    }

    const receiveReminders = (remindersReceived: ReminderServer[]) => {
        const remindersParse: Reminder [] =
            remindersReceived.map(reminder => parseCalendarToMilliseconds(reminder))

        setReminders(remindersParse)
    }

    const addedReminder = (reminder: string) => {
        const newReminder: Reminder = parseCalendarToMilliseconds(JSON.parse(reminder))

        setReminders(
            (reminders: Reminder[] | undefined) =>
                [
                    ...reminders!,
                    newReminder
                ]
        )
    }

    const updatedReminder = (reminder: string) => {
        setReminders((reminders: Reminder[] | undefined) =>
            [
                ...reminders!.filter((r: Reminder) => r.id !== JSON.parse(reminder).id),
                parseCalendarToMilliseconds(JSON.parse(reminder))
            ]
        )
    }

    const deletedReminder = (reminder: ReminderServer) => {
        setReminders((reminders: Reminder[] | undefined) =>
            reminders?.filter((r: Reminder) => r.id !== reminder.id))
    }

    const changeUsernameSuccess = (value: string) => {
        setUser((user: User | undefined | null) => ({...user!, username: value}))
        updateUser(value)
    }
}