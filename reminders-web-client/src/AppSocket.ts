import {io, Socket} from 'socket.io-client'

export const appSocket : Socket = io(process.env.REACT_APP_SOCKET_URL || "http://localhost:3000")