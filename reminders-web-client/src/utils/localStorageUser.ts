import {Reminder, ReminderServer, FormUser} from "../Types";
import moment from "moment";

const USER_USERNAME = "user_username"
const USER_PASSWORD = "user_password"

export const localStorageUser = () => {
    const saveUser = (user : FormUser) => {
        localStorage.setItem(USER_USERNAME, user.username)
        localStorage.setItem(USER_PASSWORD, user.password)
    }

    const loadUser = () : FormUser | null => {
        let user : FormUser | null = null
        const username = localStorage.getItem(USER_USERNAME)
        const password = localStorage.getItem(USER_PASSWORD)
        if (username && password) {
            user = {username, password}
        }

        return user
    }

    const removeUser = () => {
        localStorage.removeItem(USER_USERNAME)
        localStorage.removeItem(USER_PASSWORD)
    }

    const updateUser = (username : string) => {
        localStorage.setItem(USER_USERNAME, username)
    }

    return {loadUser, saveUser, removeUser, updateUser};
};