import {FormProfile, LoginResponse, Reminder, User, FormUser} from "../Types";
import {SocketEvevts} from "../enum/socketEvevts";
import {parseMillisecondsToCalendar} from "./datesParse";
import {localStorageUser} from "./localStorageUser";
import {Socket} from "socket.io-client";

export const socketEmit = (
    socket: Socket,
    reminders: Reminder[] | undefined,
    setReminders: Function,
    user: User | undefined | null,
    setUser: Function
) => {
    const { saveUser, removeUser} = localStorageUser()

    const tryLogin = (user: FormUser) => {
        socket.emit(SocketEvevts.LOGIN, JSON.stringify(user), (response: LoginResponse, user: User) => {
            response === SocketEvevts.LOGIN_SUCCESS ? loginSuccess(user) : alert("failed")
        })
    };

    const loginSuccess = (user: User) => {
        setUser(user)
        saveUser(user)
        socket.emit(SocketEvevts.GET_REMINDERS, JSON.stringify(user))
    }

    const tryLogout = () => {
        socket.emit(SocketEvevts.LOGOUT)
        removeUser()
        setUser(null)
        setReminders(undefined)
    };

    const deleteReminder = (id: string) => {
        const reminder = reminders?.find((r: Reminder) => r.id === id)
        if (reminder)
            socket.emit(SocketEvevts.DELETE_REMINDER, JSON.stringify(parseMillisecondsToCalendar(reminder)))
    };

    const addReminder = (reminder: Reminder) => {
        socket.emit(SocketEvevts.ADD_REMINDER, JSON.stringify(parseMillisecondsToCalendar(reminder)))
    };

    const updateReminder = (reminder: Reminder) => {
        socket.emit(SocketEvevts.UPDATE_REMINDER, JSON.stringify(parseMillisecondsToCalendar(reminder)))
    };

    const updateProfile = (username: FormProfile) => {
        socket.emit(SocketEvevts.CHANGE_USERNAME, JSON.stringify([user, username.username]), () => {
        })
    };

    return {tryLogin, tryLogout, deleteReminder, addReminder, updateReminder, updateProfile}
}