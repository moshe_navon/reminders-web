import {FormReminder, Reminder, ReminderServer} from "../Types";
import moment from "moment";

export const parseMillisecondsToCalendar = (reminder: Reminder): ReminderServer => {
        const momentDate : moment.Moment = moment(reminder.calendar)
        const millisecondsDate = {
            year: momentDate.year(),
            month: momentDate.month(),
            dayOfMonth: momentDate.date(),
            hourOfDay: momentDate.hour(),
            minute: momentDate.minute(),
            seconds: 0,
        }

        return {...reminder, calendar: millisecondsDate}
    }

export const parseCalendarToMilliseconds = (reminder: ReminderServer) : Reminder => {
        const millisecondsDate: number = moment({
            y: reminder.calendar.year,
            M: reminder.calendar.month,
            d: reminder.calendar.dayOfMonth,
            h: reminder.calendar.hourOfDay,
            m: reminder.calendar.minute,
            s: 0,
            ms: 0
        }).valueOf()

        return {...reminder, calendar: millisecondsDate}
    }


export const parseDateAndTimeToMomentObj = (date: string, time: string): any => {
        const dateSplit: string[] = date.split("-")
        const timeSplit: string[] = time.split(":")

        return moment({
            y: parseInt(dateSplit[0]),
            M: parseInt(dateSplit[1]) - 1,
            d: parseInt(dateSplit[2]),
            h: parseInt(timeSplit[0]),
            m: parseInt(timeSplit[1]),
        })
    }

export const isPastDateReminderForm = (reminderForm: FormReminder): Boolean => {
    const date: moment.Moment = parseDateAndTimeToMomentObj(reminderForm.date, reminderForm.time)
    return date.isBefore(moment())
}

export const isPastDateReminder = (reminder: Reminder): Boolean => {
    const date: moment.Moment = moment(reminder.calendar)
    return date.isBefore(moment())
}